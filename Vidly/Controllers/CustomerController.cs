﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;

namespace Vidly.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            List<Customer> customerList = new List<Customer>() {
                new Customer() { Id=1,
                Name="Jose"
                },
                new Customer() {Id=2, Name="Fatima" }

            };

            customerList.Add(new Customer() { Id = 3, Name = "Esmeralda" });
            return View("Index", customerList);
        }

        public ActionResult Details(int id)
        {
            List<Customer> customerList = new List<Customer>() {
                new Customer() { Id=1,
                Name="Jose"
                },
                new Customer() {Id=2, Name="Fatima" }

            };

            customerList.Add(new Customer() { Id = 3, Name = "Esmeralda" });
            var customer = customerList.FirstOrDefault(x => x.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            //var customer = from cust in customerList
            //               where cust.Id == id
            //               select cust;



            return View("Details", customer);
        }
    }
}
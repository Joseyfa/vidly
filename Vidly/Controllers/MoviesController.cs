﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Random()
        {
            var movie = new Movie()
            {
                Name = "Shrek"
            };
            var customers = new List<Customer>
            {
                new Models.Customer { Name = "Customer 1"},
                new Models.Customer { Name = "Customer 2"},
                new Models.Customer { Name = "Customer 3"},
                new Models.Customer { Name = "Customer 4"},new Models.Customer { Name = "Customer 5"}
            };
            var viewModel = new RandomMovieViewModel() {
                Movie = movie,
                Customers = customers

            };


            //ViewData["Movie"] = movie;
            //ViewBag.Movie = movie;
            return View(viewModel);
            //return View(movie);
            //return Content("Hello nalgona");
            //return HttpNotFound();
            //return new EmptyResult();
            //return RedirectToAction("Index", "Home", new { page = 1, sortBy = "name" });
        }
        public ActionResult Edit(int id)
        {
            return Content("id=" + id);
        }
        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
            {
                pageIndex = 1;
            }
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "Name";
            }
            return Content(string.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));
        }
        [Route("movies/released/{year}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);
        }

    }
}